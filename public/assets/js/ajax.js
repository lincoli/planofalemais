$(function(){		
	var form = $('#calc');
   
    form.on('submit', function(e){
        $('.result-notax').fadeOut();
        $('.result-withtax').fadeOut();        
        e.preventDefault();                  
		var data = {};
        $('.loader').fadeIn();  
		data.ddd_from = this.ddd_from.value;
		data.ddd_to = this.ddd_to.value;
		data.minutes = this.minutes.value;
		data.plan = this.plan.value;		 
		$.ajax({
			type: 'POST', 
			data: JSON.stringify(data),
	        contentType: 'application/json',
            url: '/calculate',						
            success: function(data) {
            	var result = {}
            	result = JSON.parse(data);
                console.log((result.noTax));
            	$('.loader').fadeOut();               
                if(!result.noTax){
                    $('.result-withtax').fadeIn();
                    $(".falemais span").html(result.totalWithPlan);
                    $(".tarifa-padrao span").html(result.totalWithoutPlan);
                }
               else{
                 $('.result-notax').fadeIn();
                 $(".sem-tarifa").html(result.noTax);
               }
            }
        });		
    });				
});
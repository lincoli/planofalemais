

var express = require('express');
var bodyParser = require('body-parser');
var app = express();


// Start Servidor
app.listen(8888,function()
{
    console.log("Servidor iniciado e rodando...");
});

// Diretorios Estáticos
app.use(express.static(__dirname + "/public"));
app.use('/assets', express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.post('/calculate', function(req, res){

	// Permissao
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');

	// Tarifas
	var tarifas = {		
		'011-016': 1.90,
		'016-011': 2.90,
		'011-017': 1.70,
		'017-011': 2.70,
		'011-018': 0.90,
		'018-011': 1.90
	}

	// Dados Formulário
	var stringJson =  JSON.stringify(req.body);		
	var objJson = JSON.parse(stringJson);	

	// Variaveis
	var totalWithPlan = 0;
	var totalWithoutPlan = 0;	
	var dddFrom = objJson.ddd_from;
	var dddTo = objJson.ddd_to;
	var minutes = parseInt(objJson.minutes);
	var plan = parseInt(objJson.plan);

	var tax = tarifas[dddFrom+"-"+dddTo];

	// Validação da Tarifa 
	if(tax != null){
		totalMinutes = minutes - plan;	
		var noTax = "";	
		// Cálculo Tarifa baseada nos minutos
		if(totalMinutes == 0 ){
			totalWithPlan = totalMinutes * tax;			
		}
		else if(totalMinutes > 0){ 
			totalWithPlan = totalMinutes * ((tax*0.10)+tax);
			totalWithoutPlan = minutes * tax;
		}
		else{
			totalWithoutPlan = minutes * tax;
		}
	}
	else{
		noTax = "DDD de origem ou de destino Inválidos.";
	}

	//Resposta
	var results = {};
		results.totalWithoutPlan = totalWithoutPlan.toFixed(2).replace('.',','); 
		results.totalWithPlan = totalWithPlan.toFixed(2).replace('.',',');
		results.noTax = noTax;

	// console.log("Tarifa com Fale Mais: " + totalWithPlan);
	// console.log("Tarifa Sem Fale Mais: " + totalWithoutPlan);
	//console.log(results);

 	res.end(JSON.stringify(results));
});







